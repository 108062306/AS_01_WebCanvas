var canvas = document.getElementById('mycanvas');
var ctx = canvas.getContext('2d');
var fontsize = 12;
var fonttype = "新細明體";
var mode = -1;
var textBox = document.getElementById("textBox");
var textFlag = false;
var textContent = "";
var tx, ty;
var recordarray = new Array();
var step = -1;
ctx.fillStyle="#FFFFFF";
ctx.fillRect(0,0,canvas.width,canvas.height);
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 5;
var snapshot;


function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}
function changemode(){
  mode = document.querySelector('input[name="mode"]:checked').value;
  if (mode == 0) {
     svgIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
  </svg>`;
    
     iconUrl ='data:image/svg+xml;base64,'+ window.btoa(unescape(encodeURIComponent(svgIcon)));
    document.getElementById("mycanvas").style.cursor = 'url(' + iconUrl + ') 24 24,auto' ;
    console.log(10000);
  } else if(mode == 1) {
    svgIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eraser" viewBox="0 0 16 16">
    <path d="M8.086 2.207a2 2 0 0 1 2.828 0l3.879 3.879a2 2 0 0 1 0 2.828l-5.5 5.5A2 2 0 0 1 7.879 15H5.12a2 2 0 0 1-1.414-.586l-2.5-2.5a2 2 0 0 1 0-2.828l6.879-6.879zm2.121.707a1 1 0 0 0-1.414 0L4.16 7.547l5.293 5.293 4.633-4.633a1 1 0 0 0 0-1.414l-3.879-3.879zM8.746 13.547 3.453 8.254 1.914 9.793a1 1 0 0 0 0 1.414l2.5 2.5a1 1 0 0 0 .707.293H7.88a1 1 0 0 0 .707-.293l.16-.16z"/>
  </svg>`;
    
     iconUrl ='data:image/svg+xml;base64,'+ window.btoa(unescape(encodeURIComponent(svgIcon)));
    document.getElementById("mycanvas").style.cursor = 'url(' + iconUrl + ') 24 24,auto' ;    
    console.log(11111);
  } else if(mode == 2) {
    document.getElementById("mycanvas").style.cursor = "text";
    console.log(22222);
  } else if(mode == 3) {
    svgIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-record" viewBox="0 0 16 16">
    <path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0 1A5 5 0 1 0 8 3a5 5 0 0 0 0 10z"/>
  </svg>`;
    
     iconUrl ='data:image/svg+xml;base64,'+ window.btoa(unescape(encodeURIComponent(svgIcon)));
    document.getElementById("mycanvas").style.cursor = 'url(' + iconUrl + ') 24 24,auto' ;    
    console.log(33333);

  } else if(mode == 4) {
     svgIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play" viewBox="0 0 16 16">
     <path d="M10.804 8 5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z"/>
   </svg>`;
    
     iconUrl ='data:image/svg+xml;base64,'+ window.btoa(unescape(encodeURIComponent(svgIcon)));
    document.getElementById("mycanvas").style.cursor = 'url(' + iconUrl + ') 24 24,auto' ;    
    console.log(44444);

  } else if(mode == 5) {
    svgIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-square" viewBox="0 0 16 16">
    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
  </svg>`;
    
     iconUrl ='data:image/svg+xml;base64,'+ window.btoa(unescape(encodeURIComponent(svgIcon)));
    document.getElementById("mycanvas").style.cursor = 'url(' + iconUrl + ') 24 24,auto' ;    
    console.log(55555);

  }else{
    document.getElementById("mycanvas").style.cursor = "default";
    console.log("default");
  }
}
function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  //ctx.beginPath();
  if (mode== 1 || mode == 0) {
      //ctx.moveTo(tx, ty);  
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
  } else{
    //ctx.moveTo(tx, ty);  
    ctx.putImageData(snapshot, 0, 0);
    ctx.beginPath();
    if (mode == 3){
      //ctx.drawImage(canvasPic, 0, 0); 
      var r = Math.sqrt(Math.pow(mousePos.x - tx, 2) + Math.pow(mousePos.y - ty, 2))/2;
      //cxt.lineWidth=5;
      //cxt.strokeStyle="black";
      ctx.arc((tx+ mousePos.x)/2,(ty+mousePos.y)/2 ,r,0,Math.PI*2,false); // Outer circle
      ctx.stroke();
      
    } else if (mode == 4){
      ctx.moveTo((mousePos.x + tx)/2, ty );
      ctx.lineTo(tx , mousePos.y);
      ctx.lineTo( mousePos.x, mousePos.y);
      ctx.closePath();
      ctx.stroke();
    } else if (mode == 5){
      //canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); };
      //canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); };
      ctx.strokeRect(tx, ty, mousePos.x-tx, mousePos.y-ty); 
    }
  }
}
canvas.addEventListener('mousedown', function(evt) {
  ctx.globalCompositeOperation = (mode == 1) ? 'destination-out' : 'source-over';
  if (mode != 2) {
    var mousePos = getMousePos(canvas, evt);
    tx = mousePos.x;
    ty = mousePos.y;
    evt.preventDefault();
    if (mode == 1 || mode == 0) {
      ctx.beginPath();
      ctx.moveTo(mousePos.x,  mousePos.y);  
    }
    snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height);
    canvas.addEventListener('mousemove', mouseMove, false);
  }else if (mode == 2){
    var mousePos = getMousePos(canvas, evt);
    ctx.font =  String(fontsize) + 'px ' + fonttype;   
    ctx.fillStyle = document.getElementById('cselect').value;   

    /*    
    //1. 使用`font`設定字型。   
        ctx.font =  String(fontsize) + 'px ' + fonttype;   
        //2. 使用`fillStyle`設定字型顏色。   
        ctx.fillStyle = document.getElementById('cselect').value;   
        //3. 使用`fillText()`方法顯示字型。   
        ctx.fillText( '123', mousePos.x, mousePos.y);
        */
    if (textFlag) {
        textContent = textBox.value;
        ctx.fillText(  textContent, tx, ty);
        textFlag = false;
        textBox.style['z-index'] = 1;
        textBox.style['left']= '20px';
        textBox.style['top']= '320px';
        textBox.value = "";
    } else if (!textFlag) {
      textBox.style.font = String(fontsize) + 'px ' + fonttype;
        textFlag = true;
        textBox.value = "";
        textBox.style.left = mousePos.x + 'px';
        textBox.style.color = document.getElementById('cselect').value;
        textBox.style.top = mousePos.y+300+ 'px';
        tx = mousePos.x;
        ty = mousePos.y;
        textBox.style['z-index'] = 6;
    }
  }
});

canvas.addEventListener('mouseup', function(evt) {
  step++;
  if (step < recordarray.length) { recordarray.length = step; }
  console.log(step);
  recordarray.push(document.getElementById('mycanvas').toDataURL());
  ctx.globalCompositeOperation = 'source-over';
});

function fzchange(x){
  //var chagefz=document.getElementById("div_changesize");
  //chagefz.style.fontSize=Number(x)+ "px" ;
  fontsize = x;
}
function ffchange(y){
  //var chageff=document.getElementById("div_changesize");
  //chageff.style.fontFamily=y;
  fonttype = y;
}

canvas.addEventListener('mouseup', function() {
  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);


function rst(){
  ctx.clearRect(0, 0, canvas.width, canvas.height);

}
function changecolor(){
  ctx.strokeStyle = document.getElementById('cselect').value;

}
function changerange(){
  ctx.lineWidth = document.getElementById('rselect').value;
}
function undo(){
  if (step > 0) {
    step--;
    var canvasPic = new Image();
    canvasPic.src = recordarray[step];
    console.log(step);
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }else{
    step = -1;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
}
function redo(){
  if (step < recordarray.length-1) {
    step++;
    var canvasPic = new Image();
    canvasPic.src = recordarray[step];
    console.log(step);
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}
function down(){
  var name = document.getElementById('name').value;
  var a = document.createElement("a");
  a.href = recordarray[step];
  a.download = name;
  a.click();
}


function handleImage(e){
  var reader = new FileReader();
  reader.onload = function(event){
      var img = new Image();
      img.onload = function(){
          canvas.width = img.width;
          canvas.height = img.height;
          ctx.drawImage(img,0,0);
      }
      img.src = event.target.result;
      ctx.drawImage(img, 0, 0);
  }
  reader.readAsDataURL(e.target.files[0]);  
  step++;
  if (step < recordarray.length) { recordarray.length = step; }
  console.log(step);
  recordarray.push(document.getElementById('mycanvas').toDataURL());
}


/*
function changesize(){
  fontsize = document.getElementById('fontsize').value;
}

document.getElementById('reset').addEventListener('click', function() {
  ctx.fillStyle="#FFFFFF";
  ctx.fillRect(0,0,canvas.width,canvas.height);
}, false);
function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    ctx.strokeStyle = colors[i];
  }, false);
}

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    ctx.lineWidth = size[i];
  }, false);
}

for(var i = 0; i < colors.length; i++) {
  listener(i);
}

for(var i = 0; i < size.length; i++) {
  fontSizes(i);
}*/
