# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

我在這裡做基本功能的說明：
1. Reset：按一下能清空。
2. 長度條 ：調整粗細(draw, eraser, circle, trangle, rectangle)。
3. 顏色、顏色確定鍵：左邊選擇顏色，右邊按下selectcolor才會更改。
4. 字大小、字體：text字的變化。
5. draw：畫畫。
6. eraser：橡皮擦（去除擦掉的部分的顏色而不是用白色塗）。
7. text：按一下canvas可以打字，打完按一下旁邊就會出現在canvas上。
8. circle：畫圓。
9. triangle：畫三角形。
10. rectangle：畫長方形。
11. undo：上一步。
12. redo：下一步。
13. Image Name、downlaod：Image Name那邊可以打下載下來的檔案名，右邊按下去就可以下載了。
14. 選擇檔案：upload的部分。
15. canvas：畫布的部分。
16. Total：補一個細節的部分：模式六個必須剛開始要先按才能開始作畫，並沒有設定預設。

### Function description

沒有多做bonus function。

### Gitlab page link

   https://108062306.gitlab.io/AS_01_WebCanvas


### Others (Optional)

辛苦助教了！！！

<style>
table th{
    width: 100%;
}
</style>
